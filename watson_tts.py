from watson_developer_cloud import TextToSpeechV1
import json
import os

API_Key = 'oWaTq5MbOa4MVVurn0Cen4N-elVFl-hjb5VHLIqa1ABF'
base_url = 'https://gateway-lon.watsonplatform.net/text-to-speech/api'
url_list = ["https://gateway-lon.watsonplatform.net/text-to-speech/api/v1/voices/en-US_LisaVoice",
            "https://gateway-lon.watsonplatform.net/text-to-speech/api/v1/voices/en-US_MichaelVoice",
            "https://gateway-lon.watsonplatform.net/text-to-speech/api/v1/voices/en-GB_KateVoice",
            "https://gateway-lon.watsonplatform.net/text-to-speech/api/v1/voices/en-US_AllisonVoice"]

sentences = open('Accounts_4_new.csv', 'r').read().split(',\n')

# print(sentences)

# for text in sentences:
#     for url in url_list:
#         tts = TextToSpeechV1(
#             iam_apikey=API_Key,
#             url=url
#         )
#         # sample = TextToSpeechV1.synthesize(text=text, accept='audio/mp3')
#         sample = tts.synthesize(text=text, accept='audio/mp3')
#         print(type(sample))

# for text in sentences:
tts = TextToSpeechV1(
    iam_apikey=API_Key,
    url=base_url
)

count = 0

lang = ['en-US_AllisonVoice', 'en-US_LisaVoice', 'en-US_MichaelVoice', 'en-GB_KateVoice']

def create_audio(t, c):
    for l in lang:
        if not os._exists(t+l+'.wav'):
            with open(t+l+'.wav', 'wb') as audio_file:
                audio_file.write(
                    tts.synthesize(
                        t,
                        'audio/wav',
                        l
                    ).get_result().content)
                c = c + 1
    return c


os.chdir('watson-audio')

for text in sentences:
    print('Creating', text, '.wav')
    count = create_audio(text, count)
    print(count, ' files generated.')
    print('---------------------------------------------------------------------------------------------------------------------------------------')


# print(dir(TextToSpeechV1))

# voices = text_to_speech.list_voices().get_result()
# print(json.dumps(voices, indent=2))
