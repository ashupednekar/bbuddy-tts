from google.cloud import texttospeech

# Instantiates a client
client = texttospeech.TextToSpeechClient()

def get_google_tts(s, l):
    # Set the text input to be synthesized
    synthesis_input = texttospeech.types.SynthesisInput(text=s)

    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.types.VoiceSelectionParams(
        language_code=l,
        ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)

    # Select the type of audio file you want returned
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)

    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(synthesis_input, voice, audio_config)

    # The response's audio_content is binary.
    with open('gtts-audio/'+str(s)+'_'+str(l)+'.mp3', 'wb') as out:
        # Write the response to the output file.
        out.write(response.audio_content)
        print('Audio content written to file '+str(s)+''+str(l)+'.mp3"')
    

lang_accents = ['en-AU','en-CA','en-GH','en-GB','en-IN','en-IE','en-KE','en-KE','en-NG','en-PH','en-ZA','en-TZ','en-US']
strings = open('Accounts_2.csv', 'r').read().split(',\n')

# print(strings)

i=0
for s in strings:
    for l in lang_accents:
        print(i)
        i = i + 1
        get_google_tts(s,l)
