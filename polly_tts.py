import boto3
import os

count = 0
polly = boto3.client('polly')

voices = ['Geraint', 'Gwyneth', 'Mads', 'Naja', 'Hans', 'Marlene', 'Nicole', 'Russell', 'Amy', 'Brian', 'Emma', 'Raveena', 'Ivy', 'Joanna', 'Joey', 'Justin', 'Kendra', 'Kimberly', 'Matthew', 'Salli', 'Conchita', 'Enrique', 'Miguel', 'Penelope', 'Chantal', 'Celine', 'Lea', 'Mathieu', 'Dora', 'Karl', 'Carla', 'Giorgio', 'Mizuki', 'Liv', 'Lotte', 'Ruben', 'Ewa', 'Jacek', 'Jan', 'Maja', 'Ricardo', 'Vitoria', 'Cristiano', 'Ines', 'Carmen', 'Maxim', 'Tatyana', 'Astrid', 'Filiz', 'Vicki', 'Takumi', 'Seoyeon', 'Aditi', 'Zhiyu', 'Bianca', 'Lucia', 'Mia']
languages = ['en-AU', 'en-GB', 'en-GB-WLS', 'en-IN', 'en-US']


def polly_tts(text, c):
    # polly.synthesize_speech(Text=text, voice_id='Joana', OutputFormat='mp3')
    for l in languages:
        for v in voices:
            response = polly.synthesize_speech(Text=text, VoiceId=v, LanguageCode=l, OutputFormat='mp3', SampleRate='16000')
            body = response['AudioStream'].read()
            print('Creating file: ', name+'_'+l+'_'+v+'.mp3', '...')
            if os.path.exists(name+'_'+l+'_'+v+'.mp3'):
                print('File already cached.')
                pass
            else:
                with open(name+'_'+l+'_'+v+'.mp3', 'wb') as file:
                    file.write(body)
                    file.close()
            print(c, 'files generated.')
            c = c + 1
    return c


names = open('Accounts_5_new.csv', 'r').read().split(',\n')
print('CSV reading complete...')
# print(names)
print('----------------------------------------------------------------------------------------------------------------------')
print('Changing directory to polly-audio...')
os.chdir('polly-audio')
for name in names:
    count = polly_tts(name, count)




